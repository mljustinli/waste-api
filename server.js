const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const userRoutes = express.Router();
const PORT = 4000;
const env = process.env.DB_ENV || "dev";
const config = require("./util/config")[env];
const responseMessages = require("./util/error-messages");

let User = require("./schema/user.info.model");
let Food = require("./schema/food.model");
const { encryptKey, compareKey } = require("./util/encrypter");
const { generateToken, verifyToken } = require("./util/token");
const { log } = require("./util/logger");
const { response } = require("express");
const email = require("./email/send-email");
const moment = require("moment-timezone");
const {
    DATE_FORMAT,
    DEFAULT_MEAL_TIMES,
    DEFAULT_DAYS_BEFORE_EXPIRATION_TO_NOTIFY,
    DEFAULT_RECEIVE_EXP_DATE_REMINDERS,
    DEFAULT_RECEIVE_MEAL_TIME_REMINDERS,
} = require("./util/date");
const { SAVE_FAILED, FOOD_IS_NOT_VALID } = require("./util/error-messages");
const { MAX_TOKENS_PER_USER } = require("./util/constants");
const { isFoodValid } = require("./util/validate-food");
const { isUserValid } = require("./util/validate-create-user");
const { bundleEmail } = require("./email/bundle-email");
const { EmailSender } = require("./email/email-sender");
const errorMessages = require("./util/error-messages");

// make sure environment variables are set
if (process.env.CHECK_VARS_SET !== "set") {
    console.log(
        "ENVIRONMENT VARIABLES ARE NOT SET! PLEASE DO SO WITH THE CORRECT SCRIPT OR IN THE SETTINGS ON HEROKU."
    );
}

app.use(cors());
app.use(bodyParser.json());

// database connection
// choose database based on environment
let dbURL = config.db;
log("Connecting to database on " + env);
mongoose.connect(dbURL, { useNewUrlParser: true, useUnifiedTopology: true });
const connection = mongoose.connection;
connection.once("open", function () {
    log("MongoDB database connection established successfully");
});

// API endpoints

userRoutes.route("/").get(function (req, res) {
    res.status(200).send("Server is up.");
});

// TODO verify that create user request is coming from dev machine or gitlab web app

/**
 * Create a new user if they don't already exist
 *
 * user_information: first name, last name, email, key
 * timezone
 */
userRoutes.route("/createuser").post(async function (req, res) {
    let errorResponse = {
        success: false,
        message: "",
    };

    // verify all the required user information is there
    let validUserResponse = isUserValid(req.body);

    if (!validUserResponse.valid) {
        errorResponse.message = validUserResponse.message;
        return res.status(400).json(errorResponse);
    }

    let user = new User({ user_information: req.body.user_information });

    // check if the email exists
    let count = await User.countDocuments({
        "user_information.email": user.user_information.email,
    });
    if (count > 0) {
        errorResponse.message = responseMessages.EMAIL_IN_USE;
        return res.status(400).json(errorResponse);
    }

    // hash the key
    user.user_information.key = await encryptKey(user.user_information.key);

    // generate a default random token for the user
    let newToken = generateToken();
    user.user_tokens.push({
        value: newToken,
        creationDate: moment.utc().format(DATE_FORMAT),
    });

    // create default preferences for the user
    user.user_preferences = {
        mealTimes: DEFAULT_MEAL_TIMES,
        daysBeforeExpirationToNotify: DEFAULT_DAYS_BEFORE_EXPIRATION_TO_NOTIFY,
        emailPreferences: {
            receiveExpDateReminders: DEFAULT_RECEIVE_EXP_DATE_REMINDERS,
            receiveMealTimeReminders: DEFAULT_RECEIVE_MEAL_TIME_REMINDERS,
        },
        timezone: req.body.timezone,
    };

    user.save()
        .then((user) => {
            res.status(200).json({
                success: true,
                message: responseMessages.USER_CREATED_SUCCESSFULLY,
                user_token: user.user_tokens[0].value,
                user_id: user.id,
            });
        })
        .catch((err) => {
            errorResponse.message = responseMessages.SAVE_FAILED;
            res.status(400).json(errorResponse);
        });
});

/**
 * Log in
 *
 * @param {String} email the user's email
 * @param {String} key the login key, may or may not be right
 */
userRoutes.route("/login").post(async function (req, res) {
    let errorResponse = {
        success: false,
        message: "",
    };

    // TODO add catch for no user email and no key?

    let userMatches = await User.find({
        "user_information.email": req.body.email,
    }).catch((err) => {});

    // no matches
    if (!userMatches || userMatches.length == 0) {
        errorResponse.message = responseMessages.USER_DOES_NOT_EXIST;
        return res.status(400).json(errorResponse);
    }

    let foundUser = userMatches[0];

    // check if the key is correct
    let matching = await compareKey(
        req.body.key,
        foundUser.user_information.key
    );
    if (!matching) {
        errorResponse.message = responseMessages.PASSWORD_IS_WRONG;
        return res.status(400).json(errorResponse);
    }

    // generate a new token
    let newToken = generateToken();
    log("Signed in successfully, new token is: " + newToken);

    // save the new token to the user
    foundUser.user_tokens.push({
        value: newToken,
        creationDate: moment.utc().format(DATE_FORMAT),
    });

    // if there are more than the max allowed tokens, remove them
    if (foundUser.user_tokens.length > MAX_TOKENS_PER_USER) {
        foundUser.user_tokens.splice(
            0,
            foundUser.user_tokens.length - MAX_TOKENS_PER_USER
        );
    }

    foundUser
        .save()
        .then((foundUser) => {
            res.status(200).json({
                success: true,
                user_token: newToken,
                user_id: foundUser._id,
                message: responseMessages.USER_SIGNED_ON_SUCCESSFULLY,
            });
        })
        .catch((err) => {
            res.status(400).json({
                success: false,
                message: responseMessages.SAVE_FAILED,
            });
        });
});

/**
 * Routes beyond here require the user token
 */

/**
 * Get a user's preferences
 *
 * @param {String} user_id
 * @param {String} user_token
 */
userRoutes.route("/getprefs").post(async function (req, res) {
    let errorResponse = {
        success: false,
        message: "",
    };

    let foundUser = await User.findById(req.body.user_id).catch((err) => {});

    // check that the found user exists
    if (!foundUser) {
        errorResponse.message = responseMessages.NO_USER_WITH_ID;
        return res.status(400).json(errorResponse);
    }

    // make sure token matches
    if (!verifyToken(foundUser, req.body.user_token)) {
        errorResponse.message = responseMessages.MISMATCHED_TOKEN;
        return res.status(400).json(errorResponse);
    }

    res.status(200).json({
        success: true,
        message: responseMessages.PREFERENCES_RETRIEVED,
        user_preferences: foundUser.user_preferences,
    });
});

/**
 * Update a user's preferences
 *
 * @param {String} user_id
 * @param {String} user_token
 * @param {Object} user_preferences: mealTimes, daysBeforeExpirationToNotify, emailPreferences, timezone
 */
userRoutes.route("/updateprefs").post(async function (req, res) {
    let errorResponse = {
        success: false,
        message: "",
    };
    let foundUser = await User.findById(req.body.user_id).catch((err) => {});

    // check that the found user exists
    if (!foundUser) {
        errorResponse.message = responseMessages.NO_USER_WITH_ID;
        return res.status(400).json(errorResponse);
    }

    // make sure token matches
    if (!verifyToken(foundUser, req.body.user_token)) {
        errorResponse.message = responseMessages.MISMATCHED_TOKEN;
        return res.status(400).json(errorResponse);
    }

    // TODO verify the user preferences are structured correctly
    // pretty cursory check
    let validPrefs =
        req.body.user_preferences &&
        req.body.user_preferences.mealTimes &&
        req.body.user_preferences.daysBeforeExpirationToNotify &&
        req.body.user_preferences.emailPreferences &&
        req.body.user_preferences.timezone;

    if (validPrefs) {
        foundUser.user_preferences = req.body.user_preferences;
        foundUser
            .save()
            .then((foundUser) => {
                res.status(200).json({
                    success: true,
                    message: responseMessages.PREFERENCES_UPDATED_SUCCESSFULLY,
                });
            })
            .catch((err) => {
                errorResponse.message = responseMessages.SAVE_FAILED;
                res.status(400).json(errorResponse);
            });
    } else {
        errorResponse.message = responseMessages.PREFERENCES_INVALID;
        res.status(400).json(errorResponse);
    }
});

/**
 * Get a user's food based on id
 *
 * @param {String} user_id
 * @param {String} user_token
 */
userRoutes.route("/getfoods").post(async function (req, res) {
    let errorResponse = {
        success: false,
        message: "",
    };
    let foundUser = await User.findById(req.body.user_id).catch((err) => {});

    // make sure the token matches
    if (foundUser) {
        if (verifyToken(foundUser, req.body.user_token)) {
            res.status(200).json({
                success: true,
                user_foods: foundUser.user_foods,
            });
        } else {
            errorResponse.message = responseMessages.MISMATCHED_TOKEN;
            res.status(400).json(errorResponse);
        }
    } else {
        errorResponse.message = responseMessages.NO_USER_WITH_ID;
        res.status(400).json(errorResponse);
    }
});

/**
 * Add food to a user
 *
 * @param {String} user_id
 * @param {String} user_token
 * @param {Food} food
 */
userRoutes.route("/addfood").post(async function (req, res) {
    let errorResponse = {
        success: false,
        message: "",
    };

    let foundUser = await User.findById(req.body.user_id).catch((err) => {});

    // check that the found user exists
    if (!foundUser) {
        errorResponse.message = responseMessages.NO_USER_WITH_ID;
        return res.status(400).json(errorResponse);
    }

    // make sure token matches
    if (!verifyToken(foundUser, req.body.user_token)) {
        errorResponse.message = responseMessages.MISMATCHED_TOKEN;
        return res.status(400).json(errorResponse);
    }

    let food = new Food(req.body.food);

    // food structure validation
    let validFoodResponse = isFoodValid(food);

    // make sure the food is valid
    if (validFoodResponse.valid) {
        foundUser.user_foods.push(food);

        foundUser
            .save()
            .then((foundUser) => {
                res.status(200).json({
                    success: true,
                    updated_user_foods: foundUser.user_foods,
                    food_id: food.id,
                });
            })
            .catch((err) => {
                errorResponse.message = responseMessages.SAVE_FAILED;
                res.status(400).json(errorResponse);
            });
    } else {
        errorResponse.message = validFoodResponse.message;
        res.status(400).json(errorResponse);
    }
});

/**
 * Update a food in the user's food list
 *
 * @param {String} user_id
 * @param {String} user_token
 * @param {String} food_id
 * @param {Food} food json representation, not the model
 */
userRoutes.route("/updatefood").post(async function (req, res) {
    let errorResponse = {
        success: false,
        message: "",
    };

    let foundUser = await User.findById(req.body.user_id).catch((err) => {});

    // check that the found user exists
    if (!foundUser) {
        errorResponse.message = responseMessages.NO_USER_WITH_ID;
        return res.status(400).json(errorResponse);
    }

    // make sure token matches
    if (!verifyToken(foundUser, req.body.user_token)) {
        errorResponse.message = responseMessages.MISMATCHED_TOKEN;
        return res.status(400).json(errorResponse);
    }

    let foodToUpdateID = req.body.food_id;
    let newFood = req.body.food;

    // validate newFood
    let isFoodValidResponse = isFoodValid(newFood);
    if (!isFoodValidResponse.valid) {
        errorResponse.message = isFoodValidResponse.message;
        return res.status(400).json(errorResponse);
    }

    // make sure the food exists
    if (foodToUpdateID && newFood) {
        let updated = false;

        foundUser.user_foods.forEach((food) => {
            if (food.id === foodToUpdateID) {
                food.name = newFood.name;
                food.expDate = newFood.expDate;
                food.frequency = newFood.frequency;
                food.meals = newFood.meals;

                updated = true;
            }
        });

        if (!updated) {
            errorResponse.message =
                responseMessages.NO_MATCHING_FOOD_IN_FOOD_LIST;
            return res.status(400).json(errorResponse);
        }

        foundUser
            .save()
            .then((foundUser) => {
                res.status(200).json({
                    success: true,
                    updated_user_foods: foundUser.user_foods,
                });
            })
            .catch((err) => {
                errorResponse.message = responseMessages.SAVE_FAILED;
                res.status(400).json(errorResponse);
            });
    } else {
        errorResponse.message = responseMessages.NO_FOOD_ID_IN_REQUEST;
        res.status(400).json(errorResponse);
    }

    // // make sure the food exists
    // if (foodToRemoveID) {
    //     // remove the food in the user's food list
    //     let foodList = foundUser.user_foods;
    //     let removed = false;
    //     for (let foodIndex = 0; foodIndex < foodList.length; foodIndex++) {
    //         if (foodList[foodIndex].id === foodToRemoveID) {
    //             foundUser.user_foods.splice(foodIndex, 1); // need to directly access the user's list
    //             removed = true;
    //             break;
    //         }
    //     }

    //     if (!removed) {
    //         errorResponse.message =
    //             responseMessages.NO_MATCHING_FOOD_IN_FOOD_LIST;
    //         return res.status(400).json(errorResponse);
    //     }

    //     // add in a new food with the updated information

    //     let updatedFood;

    //     try {
    //         updatedFood = new Food(req.body.food);
    //         foundUser.user_foods.push(updatedFood);
    //     } catch (err) {
    //         errorResponse.message = responseMessages.FOOD_IS_NOT_VALID;
    //         return res.status(400).json(errorResponse);
    //     }

    //     foundUser
    //         .save()
    //         .then((foundUser) => {
    //             res.status(200).json({
    //                 success: true,
    //                 updated_user_foods: foundUser.user_foods,
    //             });
    //         })
    //         .catch((err) => {
    //             errorResponse.message = responseMessages.SAVE_FAILED;
    //             res.status(400).json(errorResponse);
    //         });
    // } else {
    //     errorResponse.message = responseMessages.NO_FOOD_ID_IN_REQUEST;
    //     res.status(400).json(errorResponse);
    // }
});

/**
 * Remove a food from a user's food list
 *
 * @param {String} user_id
 * @param {String} user_token
 * @param {Number} food_id
 */
userRoutes.route("/removefood").post(async function (req, res) {
    let errorResponse = {
        success: false,
        message: "",
    };

    let foundUser = await User.findById(req.body.user_id).catch((err) => {});

    // check that the found user exists
    if (!foundUser) {
        errorResponse.message = responseMessages.NO_USER_WITH_ID;
        return res.status(400).json(errorResponse);
    }

    // make sure token matches
    if (!verifyToken(foundUser, req.body.user_token)) {
        errorResponse.message = responseMessages.MISMATCHED_TOKEN;
        return res.status(400).json(errorResponse);
    }

    let foodToRemoveID = req.body.food_id;

    // make sure the food exists
    if (foodToRemoveID) {
        // remove the food from the user's food list
        let foodList = foundUser.user_foods;
        let removed = false;
        for (let foodIndex = 0; foodIndex < foodList.length; foodIndex++) {
            if (foodList[foodIndex].id === foodToRemoveID) {
                foundUser.user_foods.splice(foodIndex, 1); // need to directly access the user's list
                removed = true;
                break; // just need to remove 1, even if there are duplicates
            }
        }

        if (!removed) {
            errorResponse.message =
                responseMessages.NO_MATCHING_FOOD_IN_FOOD_LIST;
            return res.status(400).json(errorResponse);
        }

        foundUser
            .save()
            .then((foundUser) => {
                res.status(200).json({
                    success: true,
                    updated_user_foods: foundUser.user_foods,
                });
            })
            .catch((err) => {
                errorResponse.message = responseMessages.SAVE_FAILED;
                res.status(400).json(errorResponse);
            });
    } else {
        errorResponse.message = responseMessages.REMOVE_FOOD_WITHOUT_ID;
        res.status(400).json(errorResponse);
    }
});

/**
 * Goes through users and generates all the emails
 */
userRoutes.route("/batchemails").post(async function (req, res) {
    if (
        (process.env.DB_ENV || "dev") === "dev" ||
        (req.headers &&
            req.headers["user-agent"] &&
            req.headers["user-agent"].includes("cron-job.org") &&
            req.headers["cron_token"] === process.env.CRON_TOKEN)
    ) {
        log("Received request to send emails.");

        await bundleEmail(moment.utc(), new EmailSender()).catch((err) => {
            log("Error in sending meal reminder emails");
            return res.status(400).json({ message: "Batch email failed." });
        });

        res.status(200).send({ message: "Batch email successfully finished." });
    } else {
        return res.status(400).send({ message: "Batch email failed." });
    }
});

app.use("/user", userRoutes);

app.listen(process.env.PORT || PORT, function () {
    log("Server is running on Port: " + PORT);
});

module.exports = app.listen();
