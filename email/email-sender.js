const { sendReminderEmail } = require("../email/send-email.js");
const {
    emailWrapperStyle,
    topAndBottomStyle,
    middleStyle,
    listElementStyle,
    highlightStyle,
} = require("./email-styles.js");

class EmailSender {
    /**
     * Sends emails to users
     *
     * @param {Array<EmailInfo>} emailInfoList list of users to email with information
     * @returns whether or not it was a success
     */
    async sendMealReminderEmails(emailInfoList) {
        let success = true;
        for (let index = 0; index < emailInfoList.length; index++) {
            let emailInfo = emailInfoList[index];

            let foodHTML = "";
            emailInfo.reminderFoods.forEach((food) => {
                foodHTML += `<div style="${listElementStyle}">${food.name}</div>`;
            });

            let HTMLContent = `
            <div id="email-wrapper" style = "${emailWrapperStyle}">
                <div style="${topAndBottomStyle}">
                    Hey <strong>${emailInfo.firstName}</strong>,<br/><br/>

                    Remember to eat these foods or use these ingredients!
                </div>
                <div style="${middleStyle}">
                    ${foodHTML}
                </div>
                <div style="${topAndBottomStyle}">
                    Be sure to eat them soon!
                </div>
            </div>`;

            let emailSuccess = await sendReminderEmail(
                emailInfo.email,
                "Remember to eat these foods!",
                HTMLContent
            );

            if (!emailSuccess) {
                success = false;
            }
        }

        return success;
    }

    /**
     * Sends exp date reminder emails to users
     *
     * @param {Array<EmailInfo>} emailInfoList
     */
    async sendExpDateEmails(emailInfoList) {
        let success = true;
        for (let index = 0; index < emailInfoList.length; index++) {
            let emailInfo = emailInfoList[index];

            let foodHTML = "";
            emailInfo.reminderFoods.forEach((food) => {
                let dayOrDays =
                    Math.abs(food.daysUntilExpiration) == 1 ? "day" : "days";

                if (food.daysUntilExpiration < 0) {
                    foodHTML += `
                    <div style="${listElementStyle}">
                        <span style="${highlightStyle}">${food.name}</span> is
                        <span style="${highlightStyle}">
                            ${Math.abs(
                                food.daysUntilExpiration
                            )} ${dayOrDays} expired
                        </span>
                    </div>`;
                } else if (food.daysUntilExpiration == 0) {
                    foodHTML += `
                    <div style="${listElementStyle}">
                        <span style="${highlightStyle}">${food.name}</span> expired <span style="${highlightStyle}">today</span>
                    </div>`;
                } else {
                    foodHTML += `
                    <div style="${listElementStyle}">
                        <span style="${highlightStyle}">${food.name}</span> will expire in <span style="${highlightStyle}">${food.daysUntilExpiration} ${dayOrDays}</span>
                    </div>`;
                }
            });

            let HTMLContent = `
            <div id="email-wrapper" style="${emailWrapperStyle}">
                <div style="${topAndBottomStyle}">
                    Hey <strong>${emailInfo.firstName}</strong>,<br/><br/>

                    Your food is about to expire!
                </div>
                <div style="${middleStyle}">
                    ${foodHTML}
                </div>
                <div style="${topAndBottomStyle}">
                    Be sure to eat them soon!
                </div>
            </div>`;

            let emailSuccess = await sendReminderEmail(
                emailInfo.email,
                "Foods expiring soon!",
                HTMLContent
            );

            if (!emailSuccess) {
                success = false;
            }
        }

        return success;
    }

    async sendErrorEmail(subject, content) {
        await sendReminderEmail(process.env.EMAIL_USER, subject, content);
    }
}

module.exports = {
    EmailSender,
};
