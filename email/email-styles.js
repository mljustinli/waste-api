module.exports = {
    emailWrapperStyle: `background-color: #66CCA2; color: #FFFFFF; font-size: 18px;`,
    listElementStyle: `border-left: 4px solid #66CCA2; margin-bottom: 0.5rem; padding: 0.3rem; padding-left: 1rem; `,
    highlightStyle: `color: #66CCA2`,
    topAndBottomStyle: `background-color: #66CCA2; padding: 4rem; `,
    middleStyle: `background-color: #FFFFFF; color: #888888; padding: 4rem; `,
};
