let User = require("../schema/user.info.model");
const { DATE_FORMAT } = require("../util/date");
const { TIME_COMPARISON_BUFFER } = require("../util/constants");
const moment = require("moment-timezone");
const { MealReminderEmailInfo } = require("./meal-reminder-email-info");
const FREQUENCY_ENUM = require("../schema/frequency-enum");
const { log } = require("../util/logger");
const { sendReminderEmail } = require("./send-email");

let mealNames = ["breakfast", "lunch", "dinner"];
let weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
];

/**
 * Looks through the database given the current UTC time and
 * bundles together an email for each user.
 *
 * @param {moment} UTCTime the current time in UTC
 * @param {EmailSender} emailSender the object to use for sending emails
 */
async function bundleEmail(UTCTime, emailSender) {
    log("Starting email job...");

    let emailInfoWrapper = {
        reminderEmailInfo: [],
        expDateEmailInfo: [],
    };

    let foundUsers = await User.find({}).catch((err) => {
        return "Can't connect";
    });

    if (foundUsers === "Can't connect") {
        log(
            "Couldn't find any users in the database. Maybe the database connection failed."
        );
        await emailSender.sendErrorEmail(
            "ERROR - COULDN'T CONNECT TO DATABASE WHEN DOING EMAIL JOBS",
            ":("
        );
    } else {
        // create emails for meal reminders
        let mealReminderEmailInfoList = [];
        let expDateEmailInfoList = [];
        foundUsers.forEach((user) => {
            let mealReminderEmailInfo = generateMealReminderEmailInfoForUser(
                UTCTime,
                user
            );
            if (mealReminderEmailInfo) {
                mealReminderEmailInfoList.push(mealReminderEmailInfo);
            }

            let expDateEmailInfo = generateExpDateEmailInfoForUser(
                UTCTime,
                user
            );
            if (expDateEmailInfo) {
                expDateEmailInfoList.push(expDateEmailInfo);
            }
        });
        let mealReminderEmailSuccess = await emailSender.sendMealReminderEmails(
            mealReminderEmailInfoList
        );
        let expDateEmailSuccess = await emailSender.sendExpDateEmails(
            expDateEmailInfoList
        );
        if (!mealReminderEmailSuccess) {
            log("At least one meal reminder email failed to send.");
            // send email to waste not want not that wasn't able to send email batch at whatever time UTC
            await emailSender.sendErrorEmail(
                "ERROR - AT LEAST ONE MEAL REMINDER EMAIL FAILED TO SEND",
                ":("
            );
        } else {
            emailInfoWrapper.reminderEmailInfo = mealReminderEmailInfoList;
        }

        if (!expDateEmailSuccess) {
            log("At least one exp date email failed to send.");
            // send email to waste not want not that wasn't able to send email batch at whatever time UTC
            await emailSender.sendErrorEmail(
                "ERROR - AT LEAST ONE EXP DATE EMAIL FAILED TO SEND",
                ":("
            );
        } else {
            emailInfoWrapper.expDateEmailInfo = expDateEmailInfoList;
        }
    }

    return emailInfoWrapper;
}

/**
 * Generates information needed for an email (food names, user name, email, etc)
 *
 * @param {moment} UTCTime the current time
 * @param {User} user
 * @returns {MealReminderEmailInfo} information about the user and foods to include
 *                                  in the email
 */
function generateMealReminderEmailInfoForUser(UTCTime, user) {
    if (user.user_preferences.emailPreferences.receiveMealTimeReminders) {
        let foodList = [];

        user.user_foods.forEach((food) => {
            // iterate through each meal
            mealNames.forEach((mealName) => {
                if (food.meals[mealName]) {
                    // convert server time to user's timezone
                    let serverTimeInLocale = UTCTime.clone().tz(
                        user.user_preferences.timezone
                    );

                    // get user's meal time
                    let mealTime = user.user_preferences.mealTimes[mealName];
                    let mealTimeSplit = mealTime.split(":");
                    let mealTimeHour = mealTimeSplit[0];
                    let mealTimeMinute = mealTimeSplit[1];

                    let mealTimeInLocale = serverTimeInLocale.clone();
                    mealTimeInLocale.hour(mealTimeHour);
                    mealTimeInLocale.minute(mealTimeMinute);

                    // console.log("compare");
                    // console.log(UTCTime.format());
                    // console.log(serverTimeInLocale.format());
                    // console.log(mealTimeInLocale.format());
                    // console.log(mealTime);

                    let timeToAdd = false;

                    if (food.frequency.freq_type === FREQUENCY_ENUM.ONCE) {
                        let onceDateInLocale = moment(food.frequency.extra);

                        if (
                            onceDateInLocale.format(DATE_FORMAT) ===
                            serverTimeInLocale.format(DATE_FORMAT)
                        ) {
                            timeToAdd = true;
                        }
                    }

                    // check that the date is past/on the start date
                    if (food.frequency.freq_type === FREQUENCY_ENUM.DAILY) {
                        if (serverTimeInLocale.isAfter(food.frequency.extra)) {
                            timeToAdd = true;
                        }
                    }

                    if (food.frequency.freq_type === FREQUENCY_ENUM.WEEKLY) {
                        let localeDayOfWeek = serverTimeInLocale.day();
                        if (
                            food.frequency.extra.includes(
                                weekdays[localeDayOfWeek]
                            )
                        ) {
                            timeToAdd = true;
                        }
                    }

                    // see if they're within a reasonable threshold
                    // it might take longer to process, so it might not be the exact time
                    let duration = moment.duration(
                        mealTimeInLocale.diff(serverTimeInLocale)
                    );
                    let diffInMinutes = Math.abs(duration.asMinutes());
                    if (timeToAdd && diffInMinutes <= TIME_COMPARISON_BUFFER) {
                        // add food to the list
                        foodList.push({ name: food.name });
                    }
                }
            });
        });

        if (foodList.length == 0) {
            return null;
        }

        let emailInfo = new MealReminderEmailInfo(
            user.user_information.firstName,
            user.user_information.lastName,
            user.user_information.email,
            foodList
        );

        return emailInfo;
    }
    return null;
}

function generateExpDateEmailInfoForUser(UTCTime, user) {
    if (user.user_preferences.emailPreferences.receiveExpDateReminders) {
        let foodList = [];

        user.user_foods.forEach((food) => {
            // only do expiration date emails at breakfast
            if (food.meals.breakfast) {
                // convert server time to user's timezone
                let serverTimeInLocale = UTCTime.clone().tz(
                    user.user_preferences.timezone
                );

                // get user's meal time
                let mealTime = user.user_preferences.mealTimes.breakfast;
                let mealTimeSplit = mealTime.split(":");
                let mealTimeHour = mealTimeSplit[0];
                let mealTimeMinute = mealTimeSplit[1];

                let mealTimeInLocale = serverTimeInLocale.clone();
                mealTimeInLocale.hour(mealTimeHour);
                mealTimeInLocale.minute(mealTimeMinute);

                let expDateInLocale = moment.tz(
                    food.expDate,
                    user.user_preferences.timezone
                );

                // console.log("compare");
                // console.log(UTCTime.format());
                // console.log(serverTimeInLocale.format());
                // console.log(mealTimeInLocale.format());
                // console.log(mealTime);
                // console.log(expDateInLocale.format());

                let timeToAdd = false;
                let beyondExpired = false;

                // check if within x days of the expiration date
                let expDuration = moment.duration(
                    expDateInLocale.diff(serverTimeInLocale)
                );
                let diffInDays =
                    expDuration.asDays() < 0
                        ? Math.ceil(expDuration.asDays())
                        : Math.floor(expDuration.asDays());
                if (diffInDays < 0) {
                    beyondExpired = true;
                }
                if (
                    diffInDays <=
                    user.user_preferences.daysBeforeExpirationToNotify
                ) {
                    timeToAdd = true;
                }

                // see if they're within a reasonable threshold
                // it might take longer to process, so it might not be the exact time
                let duration = moment.duration(
                    mealTimeInLocale.diff(serverTimeInLocale)
                );
                let diffInMinutes = Math.abs(duration.asMinutes());
                if (timeToAdd && diffInMinutes <= TIME_COMPARISON_BUFFER) {
                    // add food to the list
                    foodList.push({
                        name: food.name,
                        beyondExpired: beyondExpired,
                        daysUntilExpiration: diffInDays,
                    });
                }
            }
        });

        if (foodList.length == 0) {
            return null;
        }

        let emailInfo = new MealReminderEmailInfo(
            user.user_information.firstName,
            user.user_information.lastName,
            user.user_information.email,
            foodList
        );

        return emailInfo;
    }
    return null;
}

module.exports = {
    bundleEmail,
};
