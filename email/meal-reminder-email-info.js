class MealReminderEmailInfo {
    constructor(firstName, lastName, email, reminderFoods) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.reminderFoods = reminderFoods;
    }
}

module.exports = {
    MealReminderEmailInfo,
};
