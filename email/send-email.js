const nodemailer = require("nodemailer");
const { log } = require("../util/logger");

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_KEY,
    },
});

/**
 * Sends the meal reminder email to the user
 *
 * @param {String} recipientEmail user's email
 * @param {String} subject the email's subject
 * @param {String?} htmlContent content of the email
 * @returns whether or not the email was sent successfully
 */
async function sendReminderEmail(recipientEmail, subject, htmlContent) {
    // if (process.env.DB_ENV === "dev") {
    //     return true;
    // }

    // send email with defined transport object
    let info = await transporter
        .sendMail({
            from: '"Waste" <wastenotwantnotapp@gmail.com>', // sender address
            to: recipientEmail, // list of receivers
            subject: (process.env.DB_ENV === "dev" ? "TEST - " : "") + subject, // Subject line
            html: htmlContent, // html body
        })
        .catch((err) => {
            console.log(err);
            return false;
        });

    log("Message sent: " + info.messageId);
    return true;
}

module.exports = {
    sendReminderEmail,
};
