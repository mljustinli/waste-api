var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
const moment = require("moment-timezone");
const { DATE_FORMAT } = require("../../util/date.js");
const FREQUENCY_ENUM = require("../../schema/frequency-enum.js");
const { bundleEmail } = require("../../email/bundle-email.js");
const { EmailSender } = require("../../email/email-sender.js");
const { log } = require("../../util/logger.js");

let user1 = {
    user_information: {
        firstName: "Justin",
        lastName: "User",
        email: process.env.TEST_RECIPIENT,
        key: "super_secure123",
    },
    timezone: "America/New_York",
};

let dailyBreakfastDinnerFood = {
    name: "Chicken nuggets",
    expDate: moment().add(2, "days").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.DAILY,
        extra: moment("2020-06-01").format(DATE_FORMAT),
    },
    meals: {
        breakfast: true,
        lunch: false,
        dinner: true,
    },
};

let weeklySundayThursdayLunchDinner = {
    name: "Leftover mac and cheese",
    expDate: moment("2020-06-15").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.WEEKLY,
        extra: "Sunday, Thursday",
    },
    meals: {
        breakfast: false,
        lunch: true,
        dinner: true,
    },
};

let onceBreakfastLunchDinner = {
    name: "Japanese curry",
    expDate: moment("2020-06-04").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.ONCE,
        extra: moment("2020-06-04").format(DATE_FORMAT),
    },
    meals: {
        breakfast: true,
        lunch: true,
        dinner: true,
    },
};

let onceBreakfastLunchDinner2 = {
    name: "Scallions",
    expDate: moment("2020-06-01").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.ONCE,
        extra: moment("2020-06-04").format(DATE_FORMAT),
    },
    meals: {
        breakfast: true,
        lunch: true,
        dinner: true,
    },
};

let onceBreakfastLunchDinner3 = {
    name: "Fried rice",
    expDate: moment("2020-06-06").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.ONCE,
        extra: moment("2020-06-04").format(DATE_FORMAT),
    },
    meals: {
        breakfast: true,
        lunch: true,
        dinner: true,
    },
};

describe("Test Email Sender", function () {
    it("Setup - Creates a user with certain foods.", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(user1)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        let body = response.body;

        user1.user_id = body.user_id;
        user1.user_token = body.user_token;

        // add some foods that require emails
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: dailyBreakfastDinnerFood,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: weeklySundayThursdayLunchDinner,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: onceBreakfastLunchDinner,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: onceBreakfastLunchDinner2,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: onceBreakfastLunchDinner3,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("Generates the correct emails in DST.", async function () {
        let thursday = moment.utc("2020-06-04 11:30"); // 7:30 EST
        let emailInfo = await bundleEmail(thursday, new EmailSender());
        let mealReminderEmailInfo = emailInfo.reminderEmailInfo;
        expect(mealReminderEmailInfo).to.exist;
        expect(mealReminderEmailInfo[0].reminderFoods).to.be.deep.equal([
            { name: "Chicken nuggets" },
            { name: "Japanese curry" },
            { name: "Scallions" },
            { name: "Fried rice" },
        ]);
    });

    it("Setup - Removes user1", async function () {
        let response = await User.findByIdAndDelete(user1.user_id);
        expect(response.user_information.email).to.equal(
            user1.user_information.email
        );
    });
});
