var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
const moment = require("moment");
const { DATE_FORMAT } = require("../../util/date.js");
const FREQUENCY_ENUM = require("../../schema/frequency-enum.js");
const { bundleEmail } = require("../../email/bundle-email.js");
const { EmailSender } = require("../../email/email-sender.js");
const { log } = require("../../util/logger.js");
const { Mongoose } = require("mongoose");
const { sendReminderEmail } = require("../../email/send-email.js");

let user1 = {
    user_information: {
        firstName: "Test",
        lastName: "User",
        email: "testemail1@example.com",
        key: "super_secure123",
    },
    timezone: "America/New_York",
};

let user2 = {
    user_information: {
        firstName: "Test2",
        lastName: "User2",
        email: "testemail2@example.com",
        key: "super_secure321",
    },
    timezone: "America/New_York",
};

let dailyBreakfastDinnerFood = {
    name: "dailyBreakfastDinnerFood",
    expDate: moment("2020-06-01").add(2, "days").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.DAILY,
        extra: moment("2020-06-01").format(DATE_FORMAT),
    },
    meals: {
        breakfast: true,
        lunch: false,
        dinner: true,
    },
};

let weeklySundayThursdayLunchDinner = {
    name: "weeklySundayThursdayLunchDinner",
    expDate: moment("2020-06-15").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.WEEKLY,
        extra: "Sunday, Thursday",
    },
    meals: {
        breakfast: false,
        lunch: true,
        dinner: true,
    },
};

let onceBreakfastLunchDinner = {
    name: "onceBreakfastLunchDinner",
    expDate: moment("2020-06-01").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.ONCE,
        extra: moment("2020-06-04").format(DATE_FORMAT),
    },
    meals: {
        breakfast: true,
        lunch: true,
        dinner: true,
    },
};

let dailyBreakfastLunchDinner = {
    name: "dailyBreakfastLunchDinnerExpiresSoon",
    expDate: moment("2020-06-06").format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.DAILY,
        extra: moment("2020-06-04").format(DATE_FORMAT),
    },
    meals: {
        breakfast: true,
        lunch: true,
        dinner: true,
    },
};

let mockEmailLogs = [];

let mockEmailSender = new EmailSender();
mockEmailSender.sendMealReminderEmails = async function (emailInfoList) {
    emailInfoList.forEach((emailInfo) => {
        let foodList = "";
        emailInfo.reminderFoods.forEach((food) => {
            foodList += food.name + ",";
        });
        mockEmailLogs.push(
            "Meal reminder sent to " +
                emailInfo.email +
                " with foods: " +
                foodList
        );
    });
    return true;
};
mockEmailSender.sendExpDateEmails = async function (emailInfoList) {
    emailInfoList.forEach((emailInfo) => {
        let foodList = "";
        emailInfo.reminderFoods.forEach((food) => {
            foodList +=
                "(" + food.name + ": " + food.daysUntilExpiration + "),";
        });
        mockEmailLogs.push(
            "Exp date reminder sent to " +
                emailInfo.email +
                " with foods: " +
                foodList
        );
    });
    return true;
};

let mockEmailSenderFail = new EmailSender();
mockEmailSenderFail.sendMealReminderEmails = async function (emailInfoList) {
    emailInfoList.forEach((emailInfo) => {
        let foodList = "";
        emailInfo.reminderFoods.forEach((food) => {
            foodList += food.name + ",";
        });
        mockEmailLogs.push(
            "Meal reminder failed to send to " +
                emailInfo.email +
                " with foods: " +
                foodList
        );
    });
    return false;
};
mockEmailSenderFail.sendExpDateEmails = async function (emailInfoList) {
    emailInfoList.forEach((emailInfo) => {
        let foodList = "";
        emailInfo.reminderFoods.forEach((food) => {
            foodList +=
                "(" + food.name + ": " + food.daysUntilExpiration + "),";
        });
        mockEmailLogs.push(
            "Exp date reminder failed to send to " +
                emailInfo.email +
                " with foods: " +
                foodList
        );
    });
    return false;
};
mockEmailSenderFail.sendErrorEmail = async function (subject, content) {
    mockEmailLogs.push("Error: " + subject + " with content: " + content);
};

describe("Test Bundle Email", function () {
    beforeEach(function () {
        mockEmailLogs = [];
    });

    it("Setup - Creates a user with certain foods.", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(user1)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        let body = response.body;

        user1.user_id = body.user_id;
        user1.user_token = body.user_token;

        // add some foods that require emails
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: dailyBreakfastDinnerFood,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: weeklySundayThursdayLunchDinner,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: onceBreakfastLunchDinner,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user1.user_id,
                user_token: user1.user_token,
                food: dailyBreakfastLunchDinner,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("Setup - Creates a second user with certain foods.", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(user2)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        let body = response.body;

        user2.user_id = body.user_id;
        user2.user_token = body.user_token;

        // add some foods that require emails
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user2.user_id,
                user_token: user2.user_token,
                food: dailyBreakfastDinnerFood,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user2.user_id,
                user_token: user2.user_token,
                food: weeklySundayThursdayLunchDinner,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        await request(app)
            .post("/user/addfood")
            .send({
                user_id: user2.user_id,
                user_token: user2.user_token,
                food: onceBreakfastLunchDinner,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("Generates the correct emails in DST.", async function () {
        this.timeout(4000);
        let thursday = moment.utc("2020-06-04 11:30"); // 7:30 EST
        let emailInfo = await bundleEmail(thursday, mockEmailSender);
        let mealReminderEmailInfo = emailInfo.reminderEmailInfo;
        expect(mealReminderEmailInfo).to.exist;
        expect(mealReminderEmailInfo[0].reminderFoods).to.be.deep.equal([
            { name: "dailyBreakfastDinnerFood" },
            { name: "onceBreakfastLunchDinner" },
            { name: "dailyBreakfastLunchDinnerExpiresSoon" },
        ]);
        expect(mockEmailLogs).to.be.deep.equal([
            "Meal reminder sent to testemail1@example.com with foods: dailyBreakfastDinnerFood,onceBreakfastLunchDinner,dailyBreakfastLunchDinnerExpiresSoon,",
            "Meal reminder sent to testemail2@example.com with foods: dailyBreakfastDinnerFood,onceBreakfastLunchDinner,",
            "Exp date reminder sent to testemail1@example.com with foods: (dailyBreakfastDinnerFood: -1),(onceBreakfastLunchDinner: -3),(dailyBreakfastLunchDinnerExpiresSoon: 1),",
            "Exp date reminder sent to testemail2@example.com with foods: (dailyBreakfastDinnerFood: -1),(onceBreakfastLunchDinner: -3),",
        ]);

        mockEmailLogs = [];
        thursday = moment.utc("2020-06-04 16:00"); // 12:00 EST
        emailInfo = await bundleEmail(thursday, mockEmailSender);
        mealReminderEmailInfo = emailInfo.reminderEmailInfo;
        expect(mealReminderEmailInfo).to.exist;
        expect(mealReminderEmailInfo[0].reminderFoods).to.be.deep.equal([
            { name: "weeklySundayThursdayLunchDinner" },
            { name: "onceBreakfastLunchDinner" },
            { name: "dailyBreakfastLunchDinnerExpiresSoon" },
        ]);
        expect(mockEmailLogs).to.be.deep.equal([
            "Meal reminder sent to testemail1@example.com with foods: weeklySundayThursdayLunchDinner,onceBreakfastLunchDinner,dailyBreakfastLunchDinnerExpiresSoon,",
            "Meal reminder sent to testemail2@example.com with foods: weeklySundayThursdayLunchDinner,onceBreakfastLunchDinner,",
        ]);

        mockEmailLogs = [];
        thursday = moment.utc("2020-06-04 22:00"); // 18:00 EST
        emailInfo = await bundleEmail(thursday, mockEmailSender);
        mealReminderEmailInfo = emailInfo.reminderEmailInfo;
        expect(mealReminderEmailInfo).to.exist;
        expect(mealReminderEmailInfo[0].reminderFoods).to.be.deep.equal([
            { name: "dailyBreakfastDinnerFood" },
            { name: "weeklySundayThursdayLunchDinner" },
            { name: "onceBreakfastLunchDinner" },
            { name: "dailyBreakfastLunchDinnerExpiresSoon" },
        ]);
        expect(mockEmailLogs).to.be.deep.equal([
            "Meal reminder sent to testemail1@example.com with foods: dailyBreakfastDinnerFood,weeklySundayThursdayLunchDinner,onceBreakfastLunchDinner,dailyBreakfastLunchDinnerExpiresSoon,",
            "Meal reminder sent to testemail2@example.com with foods: dailyBreakfastDinnerFood,weeklySundayThursdayLunchDinner,onceBreakfastLunchDinner,",
        ]);

        mockEmailLogs = [];
        let sunday = moment.utc("2020-06-07 16:00"); // 12:00 EST
        emailInfo = await bundleEmail(sunday, mockEmailSender);
        mealReminderEmailInfo = emailInfo.reminderEmailInfo;
        expect(mealReminderEmailInfo).to.exist;
        expect(mealReminderEmailInfo[0].reminderFoods).to.be.deep.equal([
            { name: "weeklySundayThursdayLunchDinner" },
            { name: "dailyBreakfastLunchDinnerExpiresSoon" },
        ]);
        expect(mockEmailLogs).to.be.deep.equal([
            "Meal reminder sent to testemail1@example.com with foods: weeklySundayThursdayLunchDinner,dailyBreakfastLunchDinnerExpiresSoon,",
            "Meal reminder sent to testemail2@example.com with foods: weeklySundayThursdayLunchDinner,",
        ]);
    });

    it("Generates the correct emails in DST - no emails needed.", async function () {
        this.timeout(4000);
        let thursday = moment.utc("2020-05-04 11:30"); // 7:30 EST
        let emailInfo = await bundleEmail(thursday, mockEmailSender);
        let mealReminderEmailInfo = emailInfo.reminderEmailInfo;
        expect(mealReminderEmailInfo).to.exist;
        expect(mealReminderEmailInfo).to.have.length(0);
        expect(mockEmailLogs).to.have.length(0);
    });

    it("User does not have meal reminder emails on.", async function () {
        this.timeout(4000);
        let foundUser = await User.findById(user2.user_id);
        foundUser.user_preferences.emailPreferences.receiveMealTimeReminders = false;
        await foundUser.save(); // have to wait for async save to finish

        let thursday = moment.utc("2020-06-04 11:30"); // 7:30 EST
        let emailInfo = await bundleEmail(thursday, mockEmailSender);
        let mealReminderEmailInfo = emailInfo.reminderEmailInfo;
        expect(mealReminderEmailInfo).to.exist;
        expect(mealReminderEmailInfo).to.have.length(1);
        expect(mealReminderEmailInfo[0].firstName).to.equal(
            user1.user_information.firstName
        );
        mockEmailSender.sendMealReminderEmails(mealReminderEmailInfo);

        expect(mockEmailLogs).to.be.deep.equal([
            "Meal reminder sent to testemail1@example.com with foods: dailyBreakfastDinnerFood,onceBreakfastLunchDinner,dailyBreakfastLunchDinnerExpiresSoon,",
            "Exp date reminder sent to testemail1@example.com with foods: (dailyBreakfastDinnerFood: -1),(onceBreakfastLunchDinner: -3),(dailyBreakfastLunchDinnerExpiresSoon: 1),",
            "Exp date reminder sent to testemail2@example.com with foods: (dailyBreakfastDinnerFood: -1),(onceBreakfastLunchDinner: -3),",
            "Meal reminder sent to testemail1@example.com with foods: dailyBreakfastDinnerFood,onceBreakfastLunchDinner,dailyBreakfastLunchDinnerExpiresSoon,",
        ]);

        // reset the preference
        foundUser.user_preferences.emailPreferences.receiveMealTimeReminders = true;
        await foundUser.save(); // have to wait for async save to finish
    });

    it("User does not have exp date emails on.", async function () {
        this.timeout(4000);
        let foundUser = await User.findById(user2.user_id);
        foundUser.user_preferences.emailPreferences.receiveExpDateReminders = false;
        await foundUser.save(); // have to wait for async save to finish

        let thursday = moment.utc("2020-06-04 11:30"); // 7:30 EST
        let emailInfo = await bundleEmail(thursday, mockEmailSender);
        let expDateEmailInfo = emailInfo.expDateEmailInfo;
        expect(expDateEmailInfo).to.have.length(1);
        expect(expDateEmailInfo[0].email).to.equal(
            user1.user_information.email
        );

        // reset the preference
        foundUser.user_preferences.emailPreferences.receiveExpDateReminders = true;
        await foundUser.save(); // have to wait for async save to finish
    });

    it("Generates the correct emails (failure - at least one meal reminder and exp date email failed to send)", async function () {
        this.timeout(4000);

        let thursday = moment.utc("2020-06-04 11:30"); // 7:30 EST
        await bundleEmail(thursday, mockEmailSenderFail);

        expect(mockEmailLogs).to.be.deep.equal([
            "Meal reminder failed to send to testemail1@example.com with foods: dailyBreakfastDinnerFood,onceBreakfastLunchDinner,dailyBreakfastLunchDinnerExpiresSoon,",
            "Meal reminder failed to send to testemail2@example.com with foods: dailyBreakfastDinnerFood,onceBreakfastLunchDinner,",
            "Exp date reminder failed to send to testemail1@example.com with foods: (dailyBreakfastDinnerFood: -1),(onceBreakfastLunchDinner: -3),(dailyBreakfastLunchDinnerExpiresSoon: 1),",
            "Exp date reminder failed to send to testemail2@example.com with foods: (dailyBreakfastDinnerFood: -1),(onceBreakfastLunchDinner: -3),",
            "Error: ERROR - AT LEAST ONE MEAL REMINDER EMAIL FAILED TO SEND with content: :(",
            "Error: ERROR - AT LEAST ONE EXP DATE EMAIL FAILED TO SEND with content: :(",
        ]);
    });

    it("Setup - Removes user1", async function () {
        let response = await User.findByIdAndDelete(user1.user_id);
        expect(response.user_information.email).to.equal(
            user1.user_information.email
        );
    });

    it("Setup - Removes user2", async function () {
        let response = await User.findByIdAndDelete(user2.user_id);
        expect(response.user_information.email).to.equal(
            user2.user_information.email
        );
    });
});
