var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");

describe("Test Batch Emails", function () {
    it("Goes through users and send emails", async function () {
        await request(app)
            .post("/user/batchemails")
            .send({})
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("Make a request to /batchemails but doesn't have the right token", async function () {
        process.env.DB_ENV = "wrongenvironment";
        await request(app)
            .post("/user/batchemails")
            .send({})
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
        process.env.DB_ENV = "dev";
    });
});
