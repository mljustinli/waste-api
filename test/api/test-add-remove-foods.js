var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
let Food = require("../../schema/food.model");
const FREQUENCY_ENUM = require("../../schema/frequency-enum.js");
const { foodListEqual } = require("../util/test-food-list-equal.js");

let newUser = {
    user_information: {
        firstName: "Another",
        lastName: "User",
        email: "addremovefoods@example.com",
        key: "super_secure123",
    },
    timezone: "America/New_York",
};

let newUserID = "";
let newToken = "";

let expectedList = [];

let pizza = {
    name: "Pizza",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.ONCE,
        extra: "2020-07-22",
    },
    meals: {
        breakfast: true,
        lunch: true,
        dinner: true,
    },
};
let firstPizzaID = 0;
let secondPizzaID = 0;
let springRoll = {
    name: "Spring Roll",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.WEEKLY,
        extra: "Monday",
    },
    meals: {
        breakfast: true,
        lunch: false,
        dinner: true,
    },
};
let chickenNuggets = {
    name: "Chicken Nuggets",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.WEEKLY,
        extra: "Monday",
    },
    meals: {
        breakfast: true,
        lunch: false,
        dinner: true,
    },
};
let firstChickenNuggetsID = 0;

describe("Test User Add and Remove Foods", function () {
    it("Setup - Creates test user", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        // confirm the response is valid
        let body = response.body;
        expect(body.success).to.equal(true);
        expect(body.message).to.equal(
            responseMessages.USER_CREATED_SUCCESSFULLY
        );
        expect(body.user_token).to.exist;
        expect(body.user_token).to.not.equal("");
        newToken = body.user_token;
        expect(body.user_id).to.exist;
        expect(body.user_id).to.not.equal("");
        newUserID = body.user_id;
    });

    it("Adds pizza to the user's food list", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: pizza,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        let newPizza = new Food(pizza);
        firstPizzaID = body.food_id;

        expectedList.push(newPizza);
        expect(foodListEqual(body.updated_user_foods, expectedList)).to.equal(
            true
        );
    });

    it("Adds chicken nuggets to the user's food list", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: chickenNuggets,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        let newChickenNuggets = new Food(chickenNuggets);
        firstChickenNuggetsID = body.food_id;

        expectedList.push(newChickenNuggets);
        expect(foodListEqual(body.updated_user_foods, expectedList)).to.equal(
            true
        );
    });

    it("Adds another pizza to the user's food list", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: pizza,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        let newPizza = new Food(pizza);
        secondPizzaID = body.food_id;

        expectedList.push(newPizza);
        expect(foodListEqual(body.updated_user_foods, expectedList)).to.equal(
            true
        );
    });

    it("Removes pizza from the user's food list", async function () {
        let response = await request(app)
            .post("/user/removefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food_id: firstPizzaID,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        // remove the first pizza from the expected list
        expectedList.splice(0, 1);
        expect(foodListEqual(body.updated_user_foods, expectedList)).to.equal(
            true
        );
    });

    it("Removes chicken nuggets from the user's food list", async function () {
        let response = await request(app)
            .post("/user/removefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food_id: firstChickenNuggetsID,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        // remove the first chicken nuggets from the expected list
        expectedList.splice(0, 1);
        expect(foodListEqual(body.updated_user_foods, expectedList)).to.equal(
            true
        );
    });

    it("Removes the remaining pizza from the user's food list", async function () {
        let response = await request(app)
            .post("/user/removefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food_id: secondPizzaID,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        // remove the second pizza from the expected list
        expectedList.splice(0, 1);
        expect(foodListEqual(body.updated_user_foods, expectedList)).to.equal(
            true
        );
    });

    it("Add a food to the user's food list (failure - user with id doesn't exist)", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: "doesn'texist",
                user_token: newToken,
                food: pizza,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.NO_USER_WITH_ID);
    });

    it("Add a food to the user's food list (failure - wrong token)", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: "123412342",
                food: pizza,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.MISMATCHED_TOKEN);
    });

    it("Add a food to the user's food list (failure - invalid food)", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: ["notavalidfoodohno"],
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.FOOD_HAS_NO_NAME);
    });

    it("Add a food to the user's food list (failure - invalid food again)", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: {
                    name: "Almost right food",
                },
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.FOOD_EXP_DATE_INVALID);
    });

    it("Remove a food when the food list is empty.", async function () {
        let response = await request(app)
            .post("/user/removefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food_id: 1234,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(
            responseMessages.NO_MATCHING_FOOD_IN_FOOD_LIST
        );
    });

    it("Remove a food from the user's food list (failure - user with id doesn't exist)", async function () {
        let response = await request(app)
            .post("/user/removefood")
            .send({
                user_id: "userdne",
                user_token: "",
                food_id: 1234,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.NO_USER_WITH_ID);
    });

    it("Remove a food from the user's food list (failure - wrong token)", async function () {
        let response = await request(app)
            .post("/user/removefood")
            .send({
                user_id: newUserID,
                user_token: "wrongtoken",
                food_id: 1234,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.MISMATCHED_TOKEN);
    });

    it("Remove a food from the user's food list (failure - no food id specified)", async function () {
        let response = await request(app)
            .post("/user/removefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.REMOVE_FOOD_WITHOUT_ID);
    });

    it("Setup - Removes test user", async function () {
        let response = await User.findByIdAndDelete(newUserID);
        expect(response.user_information.email).to.equal(
            newUser.user_information.email
        );
    });
});
