var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
let Food = require("../../schema/food.model");
const FREQUENCY_ENUM = require("../../schema/frequency-enum.js");
const { foodListEqual } = require("../util/test-food-list-equal.js");

let newUser = {
    user_information: {
        firstName: "Another",
        lastName: "User",
        email: "another@example.com",
        key: "super_secure123",
    },
    timezone: "America/New_York",
};
let newUserID = "";
let newToken = "";

let expectedList = [];

let pizza = {
    name: "Pizza",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.ONCE,
        extra: "2020-07-22",
    },
    meals: {
        breakfast: true,
        lunch: true,
        dinner: true,
    },
};
let firstPizzaID = 0;
let secondPizzaID = 0;
let springRoll = {
    name: "Spring Roll",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.WEEKLY,
        extra: "Monday",
    },
    meals: {
        breakfast: true,
        lunch: false,
        dinner: true,
    },
};
let chickenNuggets = {
    name: "Chicken Nuggets",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.WEEKLY,
        extra: "Monday",
    },
    meals: {
        breakfast: true,
        lunch: false,
        dinner: true,
    },
};
let firstChickenNuggetsID = 0;

describe("Test User Get Foods", function () {
    it("Setup - Creates test user", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        // confirm the response is valid
        let body = response.body;
        expect(body.success).to.equal(true);
        expect(body.message).to.equal(
            responseMessages.USER_CREATED_SUCCESSFULLY
        );
        expect(body.user_token).to.exist;
        expect(body.user_token).to.not.equal("");
        newToken = body.user_token;
        expect(body.user_id).to.exist;
        expect(body.user_id).to.not.equal("");
        newUserID = body.user_id;
    });

    it("Setup - Adds some foods to the user's food list", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: pizza,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        let newPizza = new Food(pizza);
        firstPizzaID = body.food_id;

        expectedList.push(newPizza);
        expect(foodListEqual(body.updated_user_foods, expectedList)).to.equal(
            true
        );
    });

    it("Gets the user's food list (success)", async function () {
        let response = await request(app)
            .post("/user/getfoods")
            .send({
                user_id: newUserID,
                user_token: newToken,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);
        expect(foodListEqual(body.user_foods, expectedList)).to.equal(true);
    });

    it("Gets the user's food list (failure - wrong token)", async function () {
        let response = await request(app)
            .post("/user/getfoods")
            .send({
                user_id: newUserID,
                user_token: "thewrongtoken",
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.MISMATCHED_TOKEN);
    });

    it("Gets the user's food list (failure - id doesn't exist in the database)", async function () {
        let response = await request(app)
            .post("/user/getfoods")
            .send({
                user_id: "someidthatdoesn'texist",
                user_token: "thewrongtoken",
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.NO_USER_WITH_ID);
    });

    it("Setup - Removes test user", async function () {
        let userDeleted = await User.findByIdAndDelete(newUserID);
        expect(userDeleted.user_information.email).to.equal(
            newUser.user_information.email
        );
    });
});
