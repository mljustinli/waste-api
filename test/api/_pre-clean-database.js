var expect = require("chai").expect;
var request = require("supertest");
var mongoose = require("mongoose");
const app = require("../../server.js");
const { log } = require("../../util/logger.js");

const env = process.env.DB_ENV || "dev";
const config = require("../../util/config")[env];

describe("Pre Test Database Cleanup", function () {
    before(function (done) {
        process.env.DB_ENV = "dev";

        // reset the test user database
        mongoose.connect(
            config.db,
            { useNewUrlParser: true, useUnifiedTopology: true },
            function () {
                mongoose.connection.db.dropCollection("userinfos", function (
                    err,
                    result
                ) {
                    if (err) {
                        // return done(err);
                    } else {
                        log(result);
                        expect(result).to.equal(
                            true,
                            "Database deletion result should be true."
                        );
                    }
                    done();
                });
            }
        );
    });

    // doesn't run the before unless there's a test case whoops
    it("Removes the database", function (done) {
        expect(3).to.equal(3);
        done();
    });
});
