var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
const { MAX_TOKENS_PER_USER } = require("../../util/constants.js");

let newUser = {
    user_information: {
        firstName: "Test",
        lastName: "User",
        email: "randomemail@example.com",
        key: "super_secure123",
    },
    timezone: "America/New_York",
};

let newUserID = "";

async function testLogin() {
    let response = await request(app)
        .post("/user/login")
        .send({
            email: newUser.user_information.email,
            key: newUser.user_information.key,
        })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200);

    let body = response.body;

    expect(body.success).to.equal(true);
    expect(body.user_token).to.exist;
    expect(body.user_token).to.not.equal("");
    expect(body.user_id).to.exist;
    expect(body.user_id).to.not.equal("");

    return response;
}

describe("Test User Login", function () {
    it("Setup - Creates test user", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        // confirm the response is valid
        let body = response.body;
        expect(body.success).to.equal(true);
        expect(body.message).to.equal(
            responseMessages.USER_CREATED_SUCCESSFULLY
        );
        expect(body.user_token).to.exist;
        expect(body.user_token).to.not.equal("");
        expect(body.user_id).to.exist;
        expect(body.user_id).to.not.equal("");
        newUserID = body.user_id;
    });

    it("Verifies a user's username and password (success)", async function () {
        await testLogin();
    });

    it("Verifies a user's username and password more than the max tokens allowed and retains only the latest tokens", async function () {
        // log in 4 more times
        for (let index = 0; index < 4; index++) {
            await testLogin();
        }

        // log in [max #] times to erase those old tokens
        // remember the latest [max #] tokens
        let lastTokens = [];
        for (let index = 0; index < MAX_TOKENS_PER_USER; index++) {
            let response = await testLogin();

            expect(response.body.user_token).to.exist;
            lastTokens.push(response.body.user_token);
        }

        // make sure the list length is right
        let foundUser = await User.findById(newUserID);
        expect(foundUser.user_tokens).to.have.length(MAX_TOKENS_PER_USER);

        // make sure it's the latest tokens in the database
        lastTokens.forEach((token) => {
            let inList = false;
            foundUser.user_tokens.forEach((tokenGroup) => {
                if (token === tokenGroup.value) {
                    inList = true;
                }
            });

            expect(inList).to.equal(true);
        });
    });

    it("Verifies a user's username and password (failure - user doesn't exist)", async function () {
        let response = await request(app)
            .post("/user/login")
            .send({
                email: "Idon'texistwhoops",
                key: newUser.user_information.key,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;

        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.USER_DOES_NOT_EXIST);
    });

    it("Verifies a user's username and password (failure - user exists but password is wrong)", async function () {
        let response = await request(app)
            .post("/user/login")
            .send({
                email: newUser.user_information.email,
                key: "thewrongpassword",
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;

        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.PASSWORD_IS_WRONG);
    });

    it("Setup - Removes test user", async function () {
        let response = await User.findByIdAndDelete(newUserID);
        expect(response.user_information.email).to.equal(
            newUser.user_information.email
        );
    });
});
