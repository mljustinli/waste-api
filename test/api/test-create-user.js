var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
const { USER_CREATED_SUCCESSFULLY } = require("../../util/error-messages");
const { encryptKey, compareKey } = require("../../util/encrypter.js");
const moment = require("moment");
const {
    DATE_FORMAT,
    DEFAULT_MEAL_TIMES,
    DEFAULT_DAYS_BEFORE_EXPIRATION_TO_NOTIFY,
    DEFAULT_RECEIVE_EXP_DATE_REMINDERS,
    DEFAULT_RECEIVE_MEAL_TIME_REMINDERS,
} = require("../../util/date.js");

let newUser;
let newUserID = "";

describe("Test User Creation", function () {
    beforeEach(function () {
        newUser = {
            user_information: {
                firstName: "Test",
                lastName: "User",
                email: "randomemail@example.com",
                key: "super_secure123",
            },
            timezone: "America/New_York",
        };
    });

    it("Creates user (success)", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        let body = response.body;

        // check the response
        expect(body.success).to.equal(true);
        expect(body.message).to.equal(USER_CREATED_SUCCESSFULLY);
        expect(body.user_token).to.exist;
        expect(body.user_id).to.exist;
        newUserID = body.user_id;

        // check the database for the user
        let userMatch = await User.findById(body.user_id);

        // check user_information
        expect(userMatch.user_information.firstName).to.equal(
            newUser.user_information.firstName
        );
        expect(userMatch.user_information.lastName).to.equal(
            newUser.user_information.lastName
        );
        expect(userMatch.user_information.email).to.equal(
            newUser.user_information.email
        );
        let keysMatch = await compareKey(
            newUser.user_information.key,
            userMatch.user_information.key
        );
        expect(keysMatch).to.equal(true);

        // check user_preferences
        expect(userMatch.user_preferences.mealTimes.breakfast).to.equal(
            DEFAULT_MEAL_TIMES.breakfast
        );
        expect(userMatch.user_preferences.mealTimes.lunch).to.equal(
            DEFAULT_MEAL_TIMES.lunch
        );
        expect(userMatch.user_preferences.mealTimes.dinner).to.equal(
            DEFAULT_MEAL_TIMES.dinner
        );
        expect(
            userMatch.user_preferences.daysBeforeExpirationToNotify
        ).to.equal(DEFAULT_DAYS_BEFORE_EXPIRATION_TO_NOTIFY);
        expect(
            userMatch.user_preferences.emailPreferences.receiveExpDateReminders
        ).to.equal(DEFAULT_RECEIVE_EXP_DATE_REMINDERS);
        expect(
            userMatch.user_preferences.emailPreferences.receiveMealTimeReminders
        ).to.equal(DEFAULT_RECEIVE_MEAL_TIME_REMINDERS);
        expect(userMatch.user_preferences.timezone).to.equal(newUser.timezone);

        // check user_tokens
        expect(userMatch.user_tokens).to.have.length(1);
        let token = userMatch.user_tokens[0];
        expect(token.value).to.equal(body.user_token);
        expect(moment.utc(token.creationDate).format(DATE_FORMAT)).to.equal(
            moment.utc().format(DATE_FORMAT)
        );

        // check user_foods
        expect(userMatch.user_foods).to.have.length(0);
    });

    it("Creates user (failure - email already taken)", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body.success).to.equal(false);
        expect(response.body.message).to.equal(responseMessages.EMAIL_IN_USE);
    });

    // failures where the request object is wrong

    it("Creates user (failure - request body is null)", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(null)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body.success).to.equal(false);
        expect(response.body.message).to.equal(
            responseMessages.USER_HAS_NO_INFORMATION
        );
    });

    it("Creates user (failure - no first name)", async function () {
        delete newUser.user_information.firstName;

        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body.success).to.equal(false);
        expect(response.body.message).to.equal(
            responseMessages.USER_HAS_NO_FIRST_NAME
        );
    });

    it("Creates user (failure - no last name)", async function () {
        delete newUser.user_information.lastName;

        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body.success).to.equal(false);
        expect(response.body.message).to.equal(
            responseMessages.USER_HAS_NO_LAST_NAME
        );
    });

    it("Creates user (failure - no email)", async function () {
        delete newUser.user_information.email;

        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body.success).to.equal(false);
        expect(response.body.message).to.equal(
            responseMessages.USER_HAS_NO_EMAIL
        );
    });

    it("Creates user (failure - no key)", async function () {
        delete newUser.user_information.key;

        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body.success).to.equal(false);
        expect(response.body.message).to.equal(
            responseMessages.USER_HAS_NO_KEY
        );
    });

    it("Creates user (failure - no timezone)", async function () {
        delete newUser.timezone;

        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body.success).to.equal(false);
        expect(response.body.message).to.equal(
            responseMessages.USER_HAS_NO_TIMEZONE
        );
    });

    it("Creates user (failure - invalid timezone)", async function () {
        newUser.timezone = "America/The_Appalachian_Mountains";

        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body.success).to.equal(false);
        expect(response.body.message).to.equal(
            responseMessages.USER_TIMEZONE_INVALID
        );
    });

    it("Setup - Removes test user", async function () {
        let userDeleted = await User.findByIdAndDelete(newUserID);
        expect(userDeleted.user_information.email).to.equal(
            newUser.user_information.email
        );
    });
});
