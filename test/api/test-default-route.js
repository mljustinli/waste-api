var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");

describe("Test / Endpoint", function () {
    it("Returns the right message", async function () {
        let response = await request(app)
            .get("/user")
            .expect("Content-Type", /html/)
            .expect(200);
        expect(response.text).to.equal("Server is up.");
    });
});
