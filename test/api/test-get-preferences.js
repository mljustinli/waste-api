var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
const { USER_CREATED_SUCCESSFULLY } = require("../../util/error-messages");
const { encryptKey, compareKey } = require("../../util/encrypter.js");
const moment = require("moment");
const {
    DATE_FORMAT,
    DEFAULT_MEAL_TIMES,
    DEFAULT_DAYS_BEFORE_EXPIRATION_TO_NOTIFY,
    DEFAULT_RECEIVE_EXP_DATE_REMINDERS,
    DEFAULT_RECEIVE_MEAL_TIME_REMINDERS,
} = require("../../util/date.js");

let newUser = {
    user_information: {
        firstName: "Another",
        lastName: "User",
        email: "getpreferences@example.com",
        key: "super_secure123",
    },
    timezone: "America/New_York",
};
let newUserID = "";
let newToken = "";

describe("Test Get User Preferences", function () {
    it("Creates user (success)", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        // confirm the response is valid
        let body = response.body;
        expect(body.success).to.equal(true);
        expect(body.message).to.equal(
            responseMessages.USER_CREATED_SUCCESSFULLY
        );
        expect(body.user_token).to.exist;
        expect(body.user_token).to.not.equal("");
        newToken = body.user_token;
        expect(body.user_id).to.exist;
        expect(body.user_id).to.not.equal("");
        newUserID = body.user_id;
    });

    it("Gets the user preferences (success)", async function () {
        let response = await request(app)
            .post("/user/getprefs")
            .send({
                user_id: newUserID,
                user_token: newToken,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        let body = response.body;

        expect(body.message).to.equal(responseMessages.PREFERENCES_RETRIEVED);

        // check database entry
        let foundUser = await User.findById(newUserID);
        expect(foundUser).to.exist;
        expect(foundUser.user_preferences.mealTimes.breakfast).to.equal(
            DEFAULT_MEAL_TIMES.breakfast
        );
        expect(foundUser.user_preferences.mealTimes.lunch).to.equal(
            DEFAULT_MEAL_TIMES.lunch
        );
        expect(foundUser.user_preferences.mealTimes.dinner).to.equal(
            DEFAULT_MEAL_TIMES.dinner
        );
        expect(
            foundUser.user_preferences.daysBeforeExpirationToNotify
        ).to.equal(DEFAULT_DAYS_BEFORE_EXPIRATION_TO_NOTIFY);
        expect(
            foundUser.user_preferences.emailPreferences.receiveExpDateReminders
        ).to.equal(DEFAULT_RECEIVE_EXP_DATE_REMINDERS);
        expect(
            foundUser.user_preferences.emailPreferences.receiveMealTimeReminders
        ).to.equal(DEFAULT_RECEIVE_MEAL_TIME_REMINDERS);
    });

    it("Gets the user preferences (failure - user doesn't exist)", async function () {
        let response = await request(app)
            .post("/user/getprefs")
            .send({
                user_id: "nouserid",
                user_token: newToken,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
        let body = response.body;

        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.NO_USER_WITH_ID);
    });

    it("Gets the user preferences (failure - wrong token)", async function () {
        let response = await request(app)
            .post("/user/getprefs")
            .send({
                user_id: newUserID,
                user_token: "wrongtoken",
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
        let body = response.body;

        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.MISMATCHED_TOKEN);
    });

    it("Setup - Removes test user", async function () {
        let userDeleted = await User.findByIdAndDelete(newUserID);
        expect(userDeleted.user_information.email).to.equal(
            newUser.user_information.email
        );
    });
});
