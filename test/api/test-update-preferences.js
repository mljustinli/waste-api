var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
const { USER_CREATED_SUCCESSFULLY } = require("../../util/error-messages");
const { encryptKey, compareKey } = require("../../util/encrypter.js");
const moment = require("moment");
const {
    DATE_FORMAT,
    DEFAULT_MEAL_TIMES,
    DEFAULT_DAYS_BEFORE_EXPIRATION_TO_NOTIFY,
    DEFAULT_RECEIVE_EXP_DATE_REMINDERS,
    DEFAULT_RECEIVE_MEAL_TIME_REMINDERS,
} = require("../../util/date.js");

let newUser = {
    user_information: {
        firstName: "Another",
        lastName: "User",
        email: "anotheranother@example.com",
        key: "super_secure123",
    },
    timezone: "America/New_York",
};
let newUserID = "";
let newToken = "";

describe("Test Update User Preferences", function () {
    it("Creates user (success)", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        // confirm the response is valid
        let body = response.body;
        expect(body.success).to.equal(true);
        expect(body.message).to.equal(
            responseMessages.USER_CREATED_SUCCESSFULLY
        );
        expect(body.user_token).to.exist;
        expect(body.user_token).to.not.equal("");
        newToken = body.user_token;
        expect(body.user_id).to.exist;
        expect(body.user_id).to.not.equal("");
        newUserID = body.user_id;
    });

    it("Updates the user preferences (success)", async function () {
        let newPreferences = {
            mealTimes: {
                breakfast: "11:30",
                lunch: "16:30",
                dinner: "23:30",
            },
            daysBeforeExpirationToNotify: 1,
            emailPreferences: {
                receiveExpDateReminders: false,
                receiveMealTimeReminders: true,
            },
            timezone: "America/New_York",
        };
        let response = await request(app)
            .post("/user/updateprefs")
            .send({
                user_id: newUserID,
                user_token: newToken,
                user_preferences: newPreferences,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        let body = response.body;

        expect(body.message).to.equal(
            responseMessages.PREFERENCES_UPDATED_SUCCESSFULLY
        );

        // check database entry
        let foundUser = await User.findById(newUserID);
        expect(foundUser).to.exist;
        expect(foundUser.user_preferences.mealTimes.breakfast).to.equal(
            newPreferences.mealTimes.breakfast
        );
        expect(foundUser.user_preferences.mealTimes.lunch).to.equal(
            newPreferences.mealTimes.lunch
        );
        expect(foundUser.user_preferences.mealTimes.dinner).to.equal(
            newPreferences.mealTimes.dinner
        );
        expect(
            foundUser.user_preferences.daysBeforeExpirationToNotify
        ).to.equal(newPreferences.daysBeforeExpirationToNotify);
        expect(
            foundUser.user_preferences.emailPreferences.receiveExpDateReminders
        ).to.equal(newPreferences.emailPreferences.receiveExpDateReminders);
        expect(
            foundUser.user_preferences.emailPreferences.receiveMealTimeReminders
        ).to.equal(newPreferences.emailPreferences.receiveMealTimeReminders);
        expect(foundUser.user_preferences.timezone).to.equal(
            newPreferences.timezone
        );
    });

    it("Updates the user preferences (failure - user doesn't exist)", async function () {
        let newPreferences = {
            mealTimes: {
                breakfast: "11:30",
                lunch: "16:30",
                dinner: "23:30",
            },
            daysBeforeExpirationToNotify: 1,
            emailPreferences: {
                receiveExpDateReminders: false,
                receiveMealTimeReminders: true,
            },
        };
        let response = await request(app)
            .post("/user/updateprefs")
            .send({
                user_id: "123",
                user_token: newToken,
                user_preferences: newPreferences,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
        let body = response.body;

        expect(body.message).to.equal(responseMessages.NO_USER_WITH_ID);
    });

    it("Updates the user preferences (failure - wrong token)", async function () {
        let newPreferences = {
            mealTimes: {
                breakfast: "11:30",
                lunch: "16:30",
                dinner: "23:30",
            },
            daysBeforeExpirationToNotify: 1,
            emailPreferences: {
                receiveExpDateReminders: false,
                receiveMealTimeReminders: true,
            },
        };
        let response = await request(app)
            .post("/user/updateprefs")
            .send({
                user_id: newUserID,
                user_token: "1234",
                user_preferences: newPreferences,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
        let body = response.body;

        expect(body.message).to.equal(responseMessages.MISMATCHED_TOKEN);
    });

    it("Updates the user preferences (failure - invalid preferences)", async function () {
        let newPreferences = {
            mealTimes: {
                breakfast: "11:30",
                lunch: "16:30",
                dinner: "23:30",
            },
            daysBeforeExpirationToNotify: 1,
            emailPreferences: {
                receiveExpDateReminders: false,
                receiveMealTimeReminders: true,
            },
        };
        let response = await request(app)
            .post("/user/updateprefs")
            .send({
                user_id: newUserID,
                user_token: newToken,
                user_preferences: newPreferences,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
        let body = response.body;

        expect(body.message).to.equal(responseMessages.PREFERENCES_INVALID);
    });

    it("Setup - Removes test user", async function () {
        let userDeleted = await User.findByIdAndDelete(newUserID);
        expect(userDeleted.user_information.email).to.equal(
            newUser.user_information.email
        );
    });
});
