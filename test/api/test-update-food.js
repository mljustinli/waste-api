var expect = require("chai").expect;
var request = require("supertest");
const app = require("../../server.js");
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
let Food = require("../../schema/food.model");
const FREQUENCY_ENUM = require("../../schema/frequency-enum.js");
const { foodListEqual } = require("../util/test-food-list-equal.js");
var mongoose = require("mongoose");
const env = process.env.DB_ENV || "dev";
const config = require("../../util/config")[env];
const { log } = require("../../util/logger.js");

let newUser = {
    user_information: {
        firstName: "Another",
        lastName: "User",
        email: "updatefood@example.com",
        key: "super_secure123",
    },
    timezone: "America/New_York",
};
let newUserID = "";
let newToken = "";

let pizza = {
    name: "Pizza",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.ONCE,
        extra: "2020-07-22",
    },
    meals: {
        breakfast: true,
        lunch: true,
        dinner: true,
    },
};
let firstPizzaID = "";
let springRoll = {
    name: "Spring Roll",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.WEEKLY,
        extra: "Monday",
    },
    meals: {
        breakfast: true,
        lunch: false,
        dinner: true,
    },
};
let firstSpringRollID = "";
let chickenNuggets = {
    name: "Chicken Nuggets",
    expDate: "2020-07-21",
    frequency: {
        freq_type: FREQUENCY_ENUM.WEEKLY,
        extra: "Monday",
    },
    meals: {
        breakfast: true,
        lunch: false,
        dinner: true,
    },
};
let firstChickenNuggetsID = "";

// TODO add a test user database validation check

describe("Test User Update Foods", function () {
    it("Setup - Creates test user", async function () {
        let response = await request(app)
            .post("/user/createuser")
            .send(newUser)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        // confirm the response is valid
        let body = response.body;
        expect(body.success).to.equal(true);
        expect(body.message).to.equal(
            responseMessages.USER_CREATED_SUCCESSFULLY
        );
        expect(body.user_token).to.exist;
        expect(body.user_token).to.not.equal("");
        newToken = body.user_token;
        expect(body.user_id).to.exist;
        expect(body.user_id).to.not.equal("");
        newUserID = body.user_id;
    });

    it("Setup - Adds pizza to the user's food list", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: pizza,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        firstPizzaID = body.food_id;
    });

    it("Updates pizza with new values (failure - user doesn't exist)", async function () {
        let response = await request(app)
            .post("/user/updatefood")
            .send({
                user_id: "1234",
                user_token: newToken,
                food_id: firstPizzaID,
                food: {
                    name: "Different Food",
                    expDate: "2020-07-28",
                    frequency: {
                        freq_type: FREQUENCY_ENUM.WEEKLY,
                        extra: "Wednesday",
                    },
                    meals: {
                        breakfast: false,
                        lunch: false,
                        dinner: false,
                    },
                },
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
    });

    it("Updates pizza with new values (failure - wrong token)", async function () {
        let response = await request(app)
            .post("/user/updatefood")
            .send({
                user_id: newUserID,
                user_token: "1234",
                food_id: firstPizzaID,
                food: {
                    name: "Different Food",
                    expDate: "2020-07-28",
                    frequency: {
                        type: FREQUENCY_ENUM.WEEKLY,
                        extra: "Wednesday",
                    },
                    meals: {
                        breakfast: false,
                        lunch: false,
                        dinner: false,
                    },
                },
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.MISMATCHED_TOKEN);
    });

    it("Updates pizza with new values (failure - updating a food that doesn't exist)", async function () {
        let response = await request(app)
            .post("/user/updatefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food_id: "wrongfoodid",
                food: {
                    name: "Different Food",
                    expDate: "2020-07-28",
                    frequency: {
                        freq_type: FREQUENCY_ENUM.WEEKLY,
                        extra: "Wednesday",
                    },
                    meals: {
                        breakfast: false,
                        lunch: false,
                        dinner: false,
                    },
                },
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(
            responseMessages.NO_MATCHING_FOOD_IN_FOOD_LIST
        );
    });

    it("Updates pizza with new values (failure - updated food is not valid)", async function () {
        let response = await request(app)
            .post("/user/updatefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food_id: firstPizzaID,
                food: "hola",
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.FOOD_HAS_NO_NAME);
    });

    it("Updates pizza with new values (failure - no food id in the request)", async function () {
        let response = await request(app)
            .post("/user/updatefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: {
                    name: "Different Food",
                    expDate: "2020-07-28",
                    frequency: {
                        freq_type: FREQUENCY_ENUM.WEEKLY,
                        extra: "Wednesday",
                    },
                    meals: {
                        breakfast: false,
                        lunch: false,
                        dinner: false,
                    },
                },
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);

        let body = response.body;
        expect(body.success).to.equal(false);
        expect(body.message).to.equal(responseMessages.NO_FOOD_ID_IN_REQUEST);
    });

    it("Updates pizza with new values (success)", async function () {
        let response = await request(app)
            .post("/user/updatefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food_id: firstPizzaID,
                food: {
                    name: "Different Food",
                    expDate: "2020-07-28",
                    frequency: {
                        freq_type: FREQUENCY_ENUM.WEEKLY,
                        extra: "Wednesday",
                    },
                    meals: {
                        breakfast: false,
                        lunch: false,
                        dinner: false,
                    },
                },
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);
    });

    it("Setup - Adds spring rolls to the user's food list", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: springRoll,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        firstSpringRollID = body.food_id;
    });

    it("Setup - Adds chicken nuggets to the user's food list", async function () {
        let response = await request(app)
            .post("/user/addfood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food: chickenNuggets,
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);

        firstChickenNuggetsID = body.food_id;
    });

    it("Updates spring rolls with new values (success)", async function () {
        let response = await request(app)
            .post("/user/updatefood")
            .send({
                user_id: newUserID,
                user_token: newToken,
                food_id: firstSpringRollID,
                food: {
                    name: "Totally not spring rolls",
                    expDate: "2020-07-28",
                    frequency: {
                        freq_type: FREQUENCY_ENUM.WEEKLY,
                        extra: "Thursday",
                    },
                    meals: {
                        breakfast: false,
                        lunch: true,
                        dinner: false,
                    },
                },
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);

        let body = response.body;
        expect(body.success).to.equal(true);
    });

    it("Setup - Removes test user", async function () {
        let response = await User.findByIdAndDelete(newUserID);
        expect(response.user_information.email).to.equal(
            newUser.user_information.email
        );
    });
});
