var expect = require("chai").expect;
let User = require("../../schema/user.info.model");
const responseMessages = require("../../util/error-messages");
let Food = require("../../schema/food.model");
const FREQUENCY_ENUM = require("../../schema/frequency-enum.js");
const { DATE_FORMAT } = require("../../util/date");
const { isFoodValid } = require("../../util/validate-food");
const moment = require("moment");
const {
    FOOD_IS_VALID,
    FOOD_IS_NULL,
    FOOD_HAS_NO_NAME,
    FOOD_EXP_DATE_INVALID,
    FOOD_HAS_NULL_FREQUENCY,
    FOOD_HAS_INVALID_FREQUENCY_TYPE,
    FOOD_HAS_INVALID_FREQUENCY_EXTRA,
    FOOD_HAS_INVALID_FREQUENCY_EXTRA_FOR_WEEKLY,
    FOOD_HAS_INVALID_MEALS_OBJECT,
} = require("../../util/error-messages");

let testFood = {
    name: "Pizza",
    expDate: moment([2020, 7, 21]).format(DATE_FORMAT),
    frequency: {
        freq_type: FREQUENCY_ENUM.ONCE,
        extra: moment([2020, 8, 23]).format(DATE_FORMAT),
    },
    meals: {
        breakfast: false,
        lunch: false,
        dinner: false,
    },
};

describe("Test Food Model", function () {
    beforeEach(function () {
        testFood = {
            name: "Pizza",
            expDate: moment([2020, 7, 21]).format(DATE_FORMAT),
            frequency: {
                freq_type: FREQUENCY_ENUM.ONCE,
                extra: moment([2020, 8, 23]).format(DATE_FORMAT),
            },
            meals: {
                breakfast: false,
                lunch: false,
                dinner: false,
            },
        };
    });

    it("Validates a food (success)", async function () {
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_IS_VALID);
        expect(response.valid).to.equal(true);
    });

    it("Validates a food with different meals (success)", async function () {
        testFood.meals.breakfast = true;
        testFood.meals.dinner = true;
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_IS_VALID);
        expect(response.valid).to.equal(true);
    });

    it("Validates a food with weekly reminders (success)", async function () {
        testFood.frequency.freq_type = FREQUENCY_ENUM.WEEKLY;
        testFood.frequency.extra = "Sunday, Monday, Saturday";
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_IS_VALID);
        expect(response.valid).to.equal(true);
    });

    it("Validates a food (failure - food is null)", async function () {
        let response = isFoodValid(null);
        expect(response.message).to.equal(FOOD_IS_NULL);
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has no name)", async function () {
        testFood.name = "";
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_HAS_NO_NAME);
        expect(response.valid).to.equal(false);

        delete testFood.name;
        expect(testFood.name).to.not.exist;
        response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_HAS_NO_NAME);
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has invalid expiration date)", async function () {
        testFood.expDate = "";
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_EXP_DATE_INVALID);
        expect(response.valid).to.equal(false);

        delete testFood.expDate;
        expect(testFood.expDate).to.not.exist;
        response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_EXP_DATE_INVALID);
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has invalid frequency)", async function () {
        delete testFood.frequency;
        expect(testFood.frequency).to.not.exist;
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_HAS_NULL_FREQUENCY);
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has invalid frequency type)", async function () {
        testFood.frequency.freq_type = "wrong";
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_HAS_INVALID_FREQUENCY_TYPE);
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has invalid frequency extra for once)", async function () {
        testFood.frequency.freq_type = FREQUENCY_ENUM.ONCE;
        testFood.frequency.extra = "";
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_HAS_INVALID_FREQUENCY_EXTRA);
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has invalid frequency extra for daily)", async function () {
        testFood.frequency.freq_type = FREQUENCY_ENUM.DAILY;
        testFood.frequency.extra = "";
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_HAS_INVALID_FREQUENCY_EXTRA);
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has invalid frequency extra for weekly)", async function () {
        testFood.frequency.freq_type = FREQUENCY_ENUM.WEEKLY;
        testFood.frequency.extra = "asdfasdf";
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(
            FOOD_HAS_INVALID_FREQUENCY_EXTRA_FOR_WEEKLY
        );
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has weekly frequency with repeated days)", async function () {
        testFood.frequency.freq_type = FREQUENCY_ENUM.WEEKLY;
        testFood.frequency.extra = "Sunday, Monday, Tuesday, Wednesday, Sunday";
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(
            FOOD_HAS_INVALID_FREQUENCY_EXTRA_FOR_WEEKLY
        );
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has invalid meals)", async function () {
        testFood.meals = {
            chicken: "wrong",
            something: "???",
        };
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_HAS_INVALID_MEALS_OBJECT);
        expect(response.valid).to.equal(false);
    });

    it("Validates a food (failure - food has meals with correct field names but invalid types)", async function () {
        testFood.meals = {
            breakfast: "thing",
            lunch: true,
            dinner: 3,
        };
        let response = isFoodValid(testFood);
        expect(response.message).to.equal(FOOD_HAS_INVALID_MEALS_OBJECT);
        expect(response.valid).to.equal(false);
    });
});
