const { encryptKey, compareKey } = require("../../util/encrypter");
const { log } = require("../../util/logger");

var expect = require("chai").expect;

/**
 * Criteria
 *
 * Given a key it should return a non null non empty string
 * Given a key and a matching hashKey it should return matching
 * Given a non matching key and a hashKey it should return not matching
 */

describe("Test Encrypter", function () {
    it("Encrypts a key (success)", async function () {
        let expected =
            "$2b$10$8h8lqVLFK6anVhwUdj5RjOIaan0AwtVOeG9fPu9usrf2qq.NwPTke";
        let key = "anewkey2235";

        let enryptedKey = await encryptKey(key);

        // won't be an exact match because there's salt, so have to use the bcrypt compare
        let isMatching = await compareKey(key, expected);
        expect(isMatching).to.equal(true);
    });

    it("Encrypts a key (failure - null key)", async function () {
        let key = null;
        return encryptKey(key).then(
            () => Promise.reject(new Error("Expected method to reject.")),
            (err) => expect(err).to.exist
        );
    });

    it("Compares a plain text key to a hashed key (success)", async function () {
        let hashedKey =
            "$2b$10$8h8lqVLFK6anVhwUdj5RjOIaan0AwtVOeG9fPu9usrf2qq.NwPTke";
        let key = "anewkey2235";

        let isMatching = await compareKey(key, hashedKey);
        expect(isMatching).to.equal(true);
    });

    it("Compares a plain text key to a hashed key (failure - not matching)", async function () {
        let hashedKey = "$2b$10$wUdj5RjOIaan0AwtVOeG9fPu9usrf2qq.NwPTke";
        let key = "anewkey2235";

        let isMatching = await compareKey(key, hashedKey);
        expect(isMatching).to.equal(false);
    });

    it("Compares a plain text key to a hashed key (failure - bcrypt error)", async function () {
        let hashedKey = "$2b$10$wUdj5RjOIaan0AwtVOeG9fPu9usrf2qq.NwPTke";
        let key = null;

        return compareKey(key, hashedKey).then(
            () => Promise.reject(new Error("Expected method to reject.")),
            (err) => expect(err).to.exist
        );
    });
});
