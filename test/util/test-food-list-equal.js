const { expect } = require("chai");

function foodListEqual(responseList, expectedList) {
    if (responseList.length != expectedList.length) {
        return false;
    }

    let matching = true;
    for (let index = 0; index < responseList.length; index++) {
        if (!foodEqual(responseList[index], expectedList[index])) {
            matching = false;
            break;
        }
    }
    return matching;
}

function foodEqual(responseFood, expectedFood) {
    return (
        responseFood.expDate === expectedFood.expDate &&
        responseFood.meals.breakfast == expectedFood.meals.breakfast &&
        responseFood.meals.lunch == expectedFood.meals.lunch &&
        responseFood.meals.dinner == expectedFood.meals.dinner
    );
}

module.exports = {
    foodListEqual,
};
