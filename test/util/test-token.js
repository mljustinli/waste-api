const { encryptKey, compareKey } = require("../../util/encrypter");
const { generateToken } = require("../../util/token");

var expect = require("chai").expect;

/**
 * Criteria
 *
 * Given a key it should return a non null non empty string
 * Given a key and a matching hashKey it should return matching
 * Given a non matching key and a hashKey it should return not matching
 */

describe("Test Token Generation", function () {
    it("Generates a random token", function () {
        let newToken = generateToken();
        expect(newToken).to.exist;
        expect(newToken).to.not.equal("");
    });
});
