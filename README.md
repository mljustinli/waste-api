# Running the backend

`nodemon server.js`

# Running the tests

`npm test`

# Running the tests with coverage data

`npm run coverage`

# Heroku

## Login

`heroku login`

## Update Heroku

`git push heroku master`

## Show logs

`heroku logs --tail`

# Troubleshooting

If you have trouble connecting to the database in the dev environment, make sure to set the DB_USER and DB_KEY environment variables or else it can't authenticate the DB user.

`source set_dev_vars.sh` if you have the shell script.
