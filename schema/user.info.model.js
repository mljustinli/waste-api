const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let Food = require("./food.model");

let User = new Schema({
    user_information: {
        firstName: String,
        lastName: String,
        email: String,
        key: String,
    },
    user_preferences: {
        mealTimes: {
            // times are in locale
            breakfast: String,
            lunch: String,
            dinner: String,
        },
        daysBeforeExpirationToNotify: Number,
        emailPreferences: {
            receiveExpDateReminders: Boolean,
            receiveMealTimeReminders: Boolean,
        },
        timezone: String,
    },
    user_tokens: [
        {
            value: String,
            creationDate: Date,
        },
    ],
    user_foods: {
        type: [Food.schema],
    },
});

module.exports = mongoose.model("UserInfo", User);
