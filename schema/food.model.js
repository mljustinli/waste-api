const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let Food = new Schema({
    name: {
        type: String,
    },
    expDate: {
        type: String,
    },
    frequency: {
        freq_type: String,
        extra: String, // once: date String, daily: blank, weekly: days of the week, comma separated (Sunday, Monday, Tuesday, Wednesday... etc)
    },
    meals: {
        breakfast: Boolean,
        lunch: Boolean,
        dinner: Boolean,
    },
});

module.exports = mongoose.model("Food", Food);
