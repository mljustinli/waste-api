const FREQUENCY_ENUM = {
    ONCE: "once",
    DAILY: "daily",
    WEEKLY: "weekly",
};

module.exports = FREQUENCY_ENUM;
