module.exports = {
    MAX_TOKENS_PER_USER: 3,
    TIME_COMPARISON_BUFFER: 5, // difference allowed when comparing two times (for emails)
};
