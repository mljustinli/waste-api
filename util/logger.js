module.exports = {
    log: function (msg) {
        if (process.env.DB_ENV === "dev") {
            console.log("      > " + msg);
        } else {
            console.log(msg);
        }
    },
};
