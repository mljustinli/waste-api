module.exports = {
    prod: {
        db:
            "mongodb+srv://" +
            process.env.DB_USER +
            ":" +
            process.env.DB_KEY +
            "@foodwaste-pqauv.mongodb.net/wasteuserdb?retryWrites=true&w=majority",
    },
    dev: {
        db:
            "mongodb+srv://" +
            process.env.DB_USER +
            ":" +
            process.env.DB_KEY +
            "@foodwaste-pqauv.mongodb.net/testwasteuserdb?retryWrites=true&w=majority",
    },
};
