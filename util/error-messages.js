module.exports = {
    // General
    SAVE_FAILED: "Failed to save user information.",
    MISMATCHED_TOKEN: "Given token does not match the user's login token.",
    NO_USER_WITH_ID: "User with the given id does not exist.",
    NO_FOOD_ID_IN_REQUEST: "Food id is not specified in the request.",

    // Create User
    USER_IS_NULL: "User is null.",
    USER_IS_VALID: "User is valid.",
    USER_HAS_NO_INFORMATION: "User does not have a user information field.",
    USER_HAS_NO_FIRST_NAME: "User first name either doesn't exist or is blank.",
    USER_HAS_NO_LAST_NAME: "User last name either doesn't exist or is blank.",
    USER_HAS_NO_EMAIL: "User email either doesn't exist or is blank.",
    USER_HAS_NO_KEY: "User key either doesn't exist or is blank.",
    USER_HAS_NO_TIMEZONE: "User timezone either doesn't exist or is blank.",
    USER_TIMEZONE_INVALID: "User timezone isn't a real timezone.",
    USER_CREATED_SUCCESSFULLY: "User created successfully.",
    USERNAME_IN_USE: "Username already in use.",
    EMAIL_IN_USE: "Email already in use.",

    // Sign On
    USER_SIGNED_ON_SUCCESSFULLY: "User logged in successfully.",
    USER_DOES_NOT_EXIST: "User with these credentials does not exist.",
    PASSWORD_IS_WRONG: "Credentials are incorrect.",

    // Get Preferences
    PREFERENCES_RETRIEVED: "User preferences successfully retrieved.",

    // Update Preferences
    PREFERENCES_UPDATED_SUCCESSFULLY: "User preferences updated successfully.",
    PREFERENCES_INVALID: "Updated preferences are invalid.",

    // Get Foods

    // Update Food
    FOOD_IS_NOT_VALID: "Given food is not a valid food object.",

    // Remove Foods
    NO_MATCHING_FOOD_IN_FOOD_LIST:
        "Given food does not exist in the user's food list.",
    REMOVE_FOOD_WITHOUT_ID: "Food id is not specified in the request.",

    // Validate Foods
    FOOD_IS_VALID: "Food is valid.",
    FOOD_IS_NULL: "Food is null.",
    FOOD_HAS_NO_NAME: "Food name field either doesn't exist or is blank.",
    FOOD_EXP_DATE_INVALID: "Food expiration date doesn't exist or is invalid.",
    FOOD_HAS_NULL_FREQUENCY: "Food frequency field doesn't exist.",
    FOOD_HAS_INVALID_FREQUENCY_TYPE: "Food frequency type is invalid.",
    FOOD_HAS_INVALID_FREQUENCY_EXTRA: "Food frequency extra is invalid.",
    FOOD_HAS_INVALID_FREQUENCY_EXTRA_FOR_WEEKLY:
        "Food frequency extra is invalid for weekly frequency.",
    FOOD_HAS_INVALID_MEALS_OBJECT:
        "Food has an invalid meals field. It either doesn't exist or doesn't have the breakfast, lunch, or dinner fields.",
};
