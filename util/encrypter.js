const bcrypt = require("bcrypt");
const saltRounds = 10;

module.exports = {
    /**
     * Encrypts a key received
     *
     * @param key the plain text key
     */
    encryptKey: async function (key) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(key, saltRounds, (err, hash) => {
                if (err) {
                    reject(err);
                }
                resolve(hash);
            });
        });
    },

    /**
     *
     * @param {String} key the login attempt key
     * @param {String} hashedKey the hashed key in the database corresponding with the user
     */
    compareKey: async function (key, hashedKey) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(key, hashedKey, function (err, res) {
                if (err) {
                    reject(err);
                }
                resolve(res);
            });
        });
    },
};
