module.exports = {
    generateToken: function () {
        //http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript
        return (
            Math.random().toString(36).substring(2, 15) +
            Math.random().toString(36).substring(2, 15)
        );
    },

    verifyToken: function (foundUser, requestToken) {
        let foundTokens = foundUser.user_tokens;
        let match = false;
        foundTokens.forEach((foundToken) => {
            if (foundToken.value == requestToken) {
                match = true;
            }
        });
        return match;
    },
};
