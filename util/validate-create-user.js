const FREQUENCY_ENUM = require("../schema/frequency-enum");
const moment = require("moment-timezone");
const {
    USER_IS_NULL,
    USER_HAS_NO_INFORMATION,
    USER_HAS_NO_FIRST_NAME,
    USER_HAS_NO_LAST_NAME,
    USER_HAS_NO_EMAIL,
    USER_HAS_NO_KEY,
    USER_HAS_NO_TIMEZONE,
    USER_TIMEZONE_INVALID,
    USER_IS_VALID,
} = require("./error-messages");

/**
 * Checks that the user's fields are correct/not empty
 * @param {Object} user the request body in createuser
 * @returns {valid: Boolean, message: String}
 */
function isUserValid(user) {
    let response = { valid: false, message: "" };

    // null case handled by not having any user information field

    if (!user.user_information) {
        response.message = USER_HAS_NO_INFORMATION;
        return response;
    }

    if (
        !user.user_information.firstName ||
        user.user_information.firstName === ""
    ) {
        response.message = USER_HAS_NO_FIRST_NAME;
        return response;
    }

    if (
        !user.user_information.lastName ||
        user.user_information.lastName === ""
    ) {
        response.message = USER_HAS_NO_LAST_NAME;
        return response;
    }

    if (!user.user_information.email || user.user_information.email === "") {
        response.message = USER_HAS_NO_EMAIL;
        return response;
    }

    if (!user.user_information.key || user.user_information.key === "") {
        response.message = USER_HAS_NO_KEY;
        return response;
    }

    if (!user.timezone || typeof user.timezone !== "string") {
        response.message = USER_HAS_NO_TIMEZONE;
        return response;
    }

    if (!moment.tz.names().includes(user.timezone)) {
        response.message = USER_TIMEZONE_INVALID;
        return response;
    }

    response.valid = true;
    response.message = USER_IS_VALID;
    return response;
}

module.exports = {
    isUserValid,
};
