const FREQUENCY_ENUM = require("../schema/frequency-enum");
const moment = require("moment");
const {
    FOOD_IS_VALID,
    FOOD_IS_NULL,
    FOOD_HAS_NO_NAME,
    FOOD_EXP_DATE_INVALID,
    FOOD_HAS_NULL_FREQUENCY,
    FOOD_HAS_INVALID_FREQUENCY_TYPE,
    FOOD_HAS_INVALID_FREQUENCY_EXTRA,
    FOOD_HAS_INVALID_FREQUENCY_EXTRA_FOR_WEEKLY,
    FOOD_HAS_INVALID_MEALS_OBJECT,
} = require("./error-messages");

let weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
];

/**
 * Checks that the food's fields are correct/not empty
 * @param {Object} food a food object in json form
 * @returns {valid: Boolean, message: String}
 */
function isFoodValid(food) {
    let response = { valid: false, message: "" };

    if (!food) {
        response.message = FOOD_IS_NULL;
        return response;
    }

    if (!food.name || food.name.trim() === "") {
        response.message = FOOD_HAS_NO_NAME;
        return response;
    }

    if (!food.expDate || !moment(food.expDate).isValid()) {
        response.message = FOOD_EXP_DATE_INVALID;
        return response;
    }

    if (!food.frequency) {
        response.message = FOOD_HAS_NULL_FREQUENCY;
        return response;
    }

    if (
        !(
            food.frequency.freq_type === FREQUENCY_ENUM.ONCE ||
            food.frequency.freq_type === FREQUENCY_ENUM.DAILY ||
            food.frequency.freq_type === FREQUENCY_ENUM.WEEKLY
        )
    ) {
        response.message = FOOD_HAS_INVALID_FREQUENCY_TYPE;
        return response;
    }

    if (food.frequency.freq_type === FREQUENCY_ENUM.WEEKLY) {
        // check that the weekly extra is correct
        let weeklyExtra = food.frequency.extra;
        weekdays.forEach((day) => {
            weeklyExtra = weeklyExtra.replace(day, "");
        });
        weeklyExtra = weeklyExtra.replace(/,/g, "");
        weeklyExtra = weeklyExtra.trim();

        if (weeklyExtra !== "") {
            response.message = FOOD_HAS_INVALID_FREQUENCY_EXTRA_FOR_WEEKLY;
            return response;
        }
    }

    if (
        (food.frequency.freq_type === FREQUENCY_ENUM.ONCE &&
            !moment(food.frequency.extra).isValid()) ||
        (food.frequency.freq_type === FREQUENCY_ENUM.DAILY &&
            !moment(food.frequency.extra).isValid())
    ) {
        response.message = FOOD_HAS_INVALID_FREQUENCY_EXTRA;
        return response;
    }

    if (
        !food.meals ||
        food.meals.breakfast == null ||
        typeof food.meals.breakfast !== "boolean" ||
        food.meals.lunch == null ||
        typeof food.meals.lunch !== "boolean" ||
        food.meals.dinner == null ||
        typeof food.meals.dinner !== "boolean"
    ) {
        response.message = FOOD_HAS_INVALID_MEALS_OBJECT;
        return response;
    }

    response.valid = true;
    response.message = FOOD_IS_VALID;
    return response;
}

module.exports = {
    isFoodValid,
};
